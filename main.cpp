#include <iostream>
#include <filesystem>
#include <string>
#include <fstream>

#include "json.hpp"

#include "Config.hpp"

using namespace std;
using namespace quicktype;

int main(int argc, char *argv[])
{
    string file;

    // Check command line args for a file
    if (argc > 0)
    {
        // We have some args passed, is the first one given a file (ignore the rest)
        if (filesystem::exists(argv[1]))
        {
            file = argv[1];
            cout << "File " << file << " exists" << endl;

            // Read the file in 
            ifstream file_stream(file);
            std::string file_contents((std::istreambuf_iterator<char>(file_stream)),
                                        std::istreambuf_iterator<char>());

            // Parse the file
            Config config = nlohmann::json::parse(file_contents);

            cout << "Profile: " << config.get_profile() << endl;

            cout << "Keys: " << endl;
            for (auto key : config.get_ssh_keys())
            {
                cout << key << endl;
            }

            cout << "Servers: " << endl;
            for (auto server : config.get_servers())
            {
                cout << server.get_address() << ":" << server.get_port() << endl;
            }
        }
    }
    else
    {
        cerr << "No file given on comamnd line args" << endl;
        return 0;
    }
}