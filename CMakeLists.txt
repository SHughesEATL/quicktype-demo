cmake_minimum_required(VERSION 3.12)

##
## Project
##
project(quicktype_demo LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include_directories(lib/json/single_include/nlohmann)

##
## Libraries
##

# Nlohmann JSON
set(JSON_BuildTests OFF CACHE INTERNAL "")
add_subdirectory(lib/json)

##
## Demo Executable
##
add_executable(quicktype_demo
    main.cpp
    Config.hpp
)

