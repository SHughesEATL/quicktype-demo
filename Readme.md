
## Generating source from JSON Schema

1. Install quicktype

```
sudo npm install -g quicktype
```

2. Run quicktype against schema

```
quicktype --src-lang json --src config.json --lang c++ --out Config.hpp --top-level Config --no-boost --type-style pascal-case --member-style underscore-case --enumerator-style underscore-case --const-style west-const --source-style single-source

```

## Building demo (Linux)

1. Create build directory and navigate into it

```
mkdir build
cd build
```

2. Configure build

```
cmake ..
```

3. Build!!

```
cmake --build .
```

## Running demo

```
./quicktype_demo ../config.json
```